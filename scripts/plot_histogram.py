#import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from numpy import genfromtxt
import sys

filenames = sys.argv[1:]

for filename in filenames:
    data = genfromtxt(filename, delimiter=',')

    widths = data[...,1] - data[...,0]
    plt.bar(data[...,0],data[...,2], width=widths, align='edge')

    #plt.xscale("log")
    #plt.yscale("log")
    #plt.ylim(bottom=5e-1)
    if "y.csv" in filename:
        plt.xlabel(r"$y$")
        plt.ylabel(r"$\mathrm{d}\sigma / \mathrm{d}y$")
        plt.savefig("y.pdf")
    else:
        plt.xlabel(r"$p_T$")
        plt.ylabel(r"$\mathrm{d}\sigma / \mathrm{d}p_T$")
        plt.savefig("pt.pdf")

    plt.close()

