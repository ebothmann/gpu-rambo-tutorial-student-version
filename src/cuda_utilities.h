#ifndef PEPPER_CUDA_UTILITIES_H
#define PEPPER_CUDA_UTILITIES_H

#include "config.h"

#ifdef  __CUDA_ARCH__
#define CUDA_CALLABLE __host__ __device__
#else
#define CUDA_CALLABLE
#endif

void cuda_set_device(int);

#if CUDA_FOUND

#define CUDA_CALL(x)                                                           \
  do {                                                                         \
    if ((x) != cudaSuccess) {                                                  \
      printf("Error at %s:%d\n", __FILE__, __LINE__);                          \
      abort();                                                                 \
    }                                                                          \
  } while (0)

#define CURAND_CALL(x)                                                         \
  do {                                                                         \
    if ((x) != CURAND_STATUS_SUCCESS) {                                        \
      printf("Error at %s:%d\n", __FILE__, __LINE__);                          \
      abort();                                                                 \
    }                                                                          \
  } while (0)

#else

#define CUDA_CALL(x)
#define CURAND_CALL(x)

#endif

#endif
