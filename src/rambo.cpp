#include "rambo.h"
#include "cuda_utilities.h"
#include "indexing.h"
#include "vec4.h"
#include <cassert>

CUDA_CALLABLE
inline int factorial(int n)
{
  assert(n >= 0);
  int ret = 1;
  for (int i = 1; i < n + 1; ++i)
    ret *= i;
  return ret;
}

void Massless_rambo::fill_momenta(std::vector<double> &p,
                                  const std::vector<double> &r, double x1,
                                  double x2) const {
  const size_t batch_size{p.size() / 4 / n_ptcl};
  Momentum_indexer<std::vector<double>> mom {p, n_ptcl, batch_size};
  for (int evt{0}; evt < batch_size; evt++) {
    mom.set_event(evt);

    // Incoming particles
    mom(0, 0) = -x1 * e_cms / 2.0;
    mom(0, 1) = 0.0;
    mom(0, 2) = 0.0;
    mom(0, 3) = -x1 * e_cms / 2.0;
    mom(1, 0) = -x2 * e_cms / 2.0;
    mom(1, 1) = 0.0;
    mom(1, 2) = 0.0;
    mom(1, 3) = x2 * e_cms / 2.0;

    // In the cms frame, p1+p2 = (e_rest,0,0,0).
    const double e_rest = sqrt(x1 * x2) * e_cms;

    // Apply trafo (3.1) in Computer Physics Communications 40 (1986) 359—373
    // and calculate sum of momenta Q to prepare the transformation in (2.5).
    Vec4 Q;
    for (int i = 2; i < n_ptcl; i++) {
      const int r_offset = (n_ptcl - 2) * 4 * evt + (i - 2) * 4;
      const double c = 2.0 * r[r_offset + 0] - 1.0;
      const double phi = 2.0 * M_PI * r[r_offset + 1];
      mom(i, 0) = -std::log(r[r_offset + 2] * r[r_offset + 3]);
      mom(i, 1) = mom(i, 0) * std::sqrt(1.0 - c * c) * std::cos(phi);
      mom(i, 2) = mom(i, 0) * std::sqrt(1.0 - c * c) * std::sin(phi);
      mom(i, 3) = mom(i, 0) * c;
      Q += Vec4(mom(i, 0), mom(i, 1), mom(i, 2), mom(i, 3));
    }

    // Apply trafo (2.5) in Computer Physics Communications 40 (1986) 359—373
    const double M = Q.abs();
    const double gamma = Q[0] / M;
    const double a = 1.0 / (1.0 + gamma);
    const double x = e_rest / M;
    const Vec3 b = Q.vec3() / (-M);

    // Apply trafo (2.4) in Computer Physics Communications 40 (1986) 359—373
    for (int i = 2; i < n_ptcl; i++) {
      const double bq = b[0] * mom(i, 1) + b[1] * mom(i, 2) + b[2] * mom(i, 3);
      const double q0 = mom(i, 0);
      mom(i, 0) = x * (gamma * q0 + bq);
      mom(i, 1) = x * (mom(i, 1) + b[0] * q0 + a * bq * b[0]);
      mom(i, 2) = x * (mom(i, 2) + b[1] * q0 + a * bq * b[1]);
      mom(i, 3) = x * (mom(i, 3) + b[2] * q0 + a * bq * b[2]);
    }

    // Boost the momenta in the respective rest frame
    const double beta = -(x1 - x2) / (x1 + x2);
    const double gamma_factor = sqrt(1. / (1. - beta * beta));
    for (int i = 2; i < n_ptcl; i++) {
      const double e = mom(i, 0);
      const double pz = mom(i, 3);
      mom(i, 0) = gamma_factor * (e - beta * pz);
      mom(i, 3) = gamma_factor * (pz - beta * e);
    }
  }
}

void Massless_rambo::fill_weights(std::vector<double> &w, double x1,
                                  double x2) const {
  // Corresponds to (2.14) in Computer Physics Communications 40 (1986)
  // 359—373
  for (int evt{0}; evt < w.size(); evt++) {
    const double e_rest = sqrt(x1 * x2) * e_cms;
    const double dlips = pow(2 * M_PI, 4) * 1. / pow(2 * M_PI, 3 * (n_ptcl - 2));
    const double gamma_product =
        static_cast<double>(factorial(n_ptcl - 3) * factorial(n_ptcl - 4));
    const double Vn = std::pow(M_PI / 2.0, n_ptcl - 3) *
                      pow(e_rest, 2 * (n_ptcl - 2) - 4) / gamma_product * dlips;
    w[evt] = Vn;
  }
}
