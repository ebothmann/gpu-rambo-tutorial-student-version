#ifndef PEPPER_VEC3_H
#define PEPPER_VEC3_H

#include "cuda_utilities.h"
#include <cmath>

class Vec3 {
public:
  CUDA_CALLABLE
  Vec3()
  {
    data[0] = 0.0;
    data[1] = 0.0;
    data[2] = 0.0;
  }
  CUDA_CALLABLE
  Vec3(double px, double py, double pz)
  {
    data[0] = px;
    data[1] = py;
    data[2] = pz;
  }
  CUDA_CALLABLE double operator[](int i) const { return data[i]; }
  CUDA_CALLABLE double &operator[](int i) { return data[i]; }
  CUDA_CALLABLE 
  double abs() const { return std::sqrt(abs2()); };
  CUDA_CALLABLE 
  double abs2() const { return (*this) * (*this); };
  CUDA_CALLABLE 
  Vec3& operator/=(double rhs)
  {
    (*this)[0] /= rhs;
    (*this)[1] /= rhs;
    (*this)[2] /= rhs;
    return *this;
  };
  CUDA_CALLABLE 
  friend Vec3 operator/(Vec3 lhs, double rhs) { return lhs /= rhs; }
  CUDA_CALLABLE 
  friend double operator*(const Vec3& lhs, const Vec3& rhs)
  {
    return lhs[0] * rhs[0] + lhs[1] * rhs[1] + lhs[2] * rhs[2];
  };

private:
  double data[3];
  
};

#endif
