#include "timing.h"
#include <iomanip>

std::map<std::string, double> Timing::detail::durations;

void Timing::print_durations(std::ostream& out)
{
  size_t label_column_width {0};
  for (auto& kv : detail::durations)
    label_column_width = std::max(label_column_width, kv.first.size());
  label_column_width++;
  out << std::left;
  out << std::setw(label_column_width) << "Task,"
      << " Duration [s]\n";
  for (auto& kv : detail::durations)
    out << std::setw(label_column_width) << (kv.first + ",") << ' ' << kv.second
        << '\n';
}
