#include "rng.h"

CUDA_CALLABLE int ri(double r, int min, int max)
{
  const double diff = static_cast<double>(max - min);
  return (int)((diff + 1.0) * r) + min;
}

CUDA_CALLABLE double rd(double r, double min, double max)
{
  return (max - min) * r + min;
}

#if DEVICE_RNG_ENABLED

Rng::Rng(int _seed_offset) : seed_offset {_seed_offset}
{
  CURAND_CALL(curandCreateGenerator(&curand_gen, CURAND_RNG_PSEUDO_DEFAULT));
  reset();
}

void Rng::reset()
{
  // reset the device rng
  CURAND_CALL(
      curandSetPseudoRandomGeneratorSeed(curand_gen, 1234ULL + seed_offset));
  CURAND_CALL(curandSetGeneratorOffset(curand_gen, 0ULL));
}

Rng::~Rng()
{
  CURAND_CALL(curandDestroyGenerator(curand_gen));
}

void Rng::fill(thrust::device_vector<double>& r)
{
  fill_d(r);
}

#else

void Rng::fill(std::vector<double>& r)
{
  fill_d(r);
}

Rng::Rng(int _seed_offset) : seed_offset(_seed_offset) { reset(); }
Rng::~Rng() {}

// It should be sufficient to use subsequent seeds, cf.
// http://dx.doi.org/10.5194/ars-12-75-2014
void Rng::reset() { std::srand(5489ULL + seed_offset); }

double Rng::d() { return static_cast<double>(std::rand()) / RAND_MAX; }

#endif
