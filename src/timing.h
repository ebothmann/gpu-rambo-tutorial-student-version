#ifndef PEPPER_TIMING_H
#define PEPPER_TIMING_H

#include <chrono>
#include <map>
#include <string>
#include <iostream>

namespace Timing {
namespace detail {
extern std::map<std::string, double> durations;
};

inline void register_duration(const std::string& label, double duration)
{
  detail::durations[label] = duration;
}

void print_durations(std::ostream&);

} // namespace Timing

class Timer {
private:
  // Type aliases to make accessing nested type easier
  using Clock = std::chrono::steady_clock;
  using Second = std::chrono::duration<double, std::ratio<1>>;

  std::chrono::time_point<Clock> m_beg {Clock::now()};

public:
  void reset() { m_beg = Clock::now(); }

  double elapsed() const
  {
    return std::chrono::duration_cast<Second>(Clock::now() - m_beg).count();
  }

  void register_elapsed(const std::string& duration_label) const
  {
    Timing::register_duration(duration_label, elapsed());
  }
};

#endif
