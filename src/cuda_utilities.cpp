#include "cuda_utilities.h"

void cuda_set_device(int i)
{
  (void)i;
#if __CUDA_ARCH__
  CUDA_CALL(cudaSetDevice(i));
#endif
}
