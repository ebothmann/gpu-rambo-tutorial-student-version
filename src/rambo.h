#include "config.h"
#include <vector>

class Massless_rambo {
public:
  Massless_rambo(double _e_cms, int _n_ptcl) : e_cms(_e_cms), n_ptcl(_n_ptcl) {}
  void fill_momenta(std::vector<double> &p, const std::vector<double> &r,
                    double x1, double x2) const;
  void fill_weights(std::vector<double> &w, double x1, double x2) const;

private:
  int n_ptcl;
  double e_cms;
};
