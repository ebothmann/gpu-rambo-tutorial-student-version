#include <vector>
#include <iostream>
#include <cmath>

class Histogram {
public:
  Histogram(double _min, double _max, int n_bins)
      : min(_min), max(_max), sum_w(n_bins, 0.0), sum_w2(n_bins, 0.0) {}
  void fill(double x, double w) {
    if (x < min) {
      underflow_sum_w += w;
      underflow_sum_w2 += w * w;
    } else if (x > max) {
      overflow_sum_w += w;
      overflow_sum_w2 += w * w;
    } else {
      int i = std::floor((x - min) / (max - min) * sum_w.size());
      sum_w[i] += w;
      sum_w2[i] += w * w;
    }
  }
  Histogram& operator/=(double fac)
  {
    for (int i = 0; i < sum_w.size(); i++) {
      sum_w[i] /= fac;
      sum_w2[i] /= fac;
    }
    underflow_sum_w /= fac;
    underflow_sum_w2 /= fac;
    overflow_sum_w /= fac;
    overflow_sum_w2 /= fac;
    return *this;
  }
  friend std::ostream &operator<<(std::ostream &o, const Histogram &v) {
    int n_bins = v.sum_w.size();
    for (int i = 0; i < n_bins; i++) {
      o << (v.min + i * (v.max - v.min) / n_bins) << ',';
      o << (v.min + (i + 1) * (v.max - v.min) / n_bins) << ',';
      o << v.sum_w[i] << ',';
      o << std::sqrt(v.sum_w[i]) << '\n';
    }
    return o;
  }

private:
  double min, max;
  std::vector<double> sum_w;
  std::vector<double> sum_w2;
  double underflow_sum_w, underflow_sum_w2;
  double overflow_sum_w, overflow_sum_w2;
};
