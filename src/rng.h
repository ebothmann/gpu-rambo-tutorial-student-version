#ifndef PEPPER_RNG_H
#define PEPPER_RNG_H

#include "config.h"
#include "cuda_utilities.h"
#include <cstdlib>
#include <vector>
#if DEVICE_RNG_ENABLED
#include <thrust/device_vector.h>
#include <cuda.h>
#include <curand.h>
#endif

// Transform a given random number to an integer between min and max,
// inclusively.
CUDA_CALLABLE int ri(double r, int min, int max);

// Transform a given random number to a double between min and max,
// inclusively.
CUDA_CALLABLE double rd(double r, double min, double max);

struct Rng {
public:
  Rng(int _seed_offset = 0);
  ~Rng();

  // fill random number storage
#if DEVICE_RNG_ENABLED
  void fill(thrust::device_vector<double>&);
#else
  void fill(std::vector<double>&);

  // generate random number in [0.0, 1.0) on the host
  double d();
#endif

  void reset();

private:

  // generic helper function for fill()
  template <typename T> void fill_d(T& v)
  {
    const size_t n = v.size();
#if DEVICE_RNG_ENABLED
    double* devData = thrust::raw_pointer_cast(&v[0]);
    CURAND_CALL(curandGenerateUniformDouble(curand_gen, devData, n));
    CUDA_CALL(cudaDeviceSynchronize());
#else
    for (size_t i = 0; i < n; ++i) {
      v[i] = d();
    }
#endif
  }

#if DEVICE_RNG_ENABLED
  curandGenerator_t curand_gen;
#endif

  int seed_offset;
};

#endif
