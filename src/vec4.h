#ifndef PEPPER_VEC4_H
#define PEPPER_VEC4_H

#include "cuda_utilities.h"
#include "vec3.h"
#include <cmath>
#include <ostream>

class Vec4 {
public:
  CUDA_CALLABLE
  Vec4()
  {
    data[0] = 0.0;
    data[1] = 0.0;
    data[2] = 0.0;
    data[3] = 0.0;
  }
  CUDA_CALLABLE
  Vec4(double e, double px, double py, double pz)
  {
    data[0] = e;
    data[1] = px;
    data[2] = py;
    data[3] = pz;
  }
  CUDA_CALLABLE double operator[](int i) const { return data[i]; }
  CUDA_CALLABLE double &operator[](int i) { return data[i]; }
  CUDA_CALLABLE 
  Vec3 vec3() const { return Vec3((*this)[1], (*this)[2], (*this)[3]); }
  // NOTE: The following signed_abs member function is not physical, but can be
  // used to print the invariant mass of four vectors. Negative values then
  // mean that the invariant mass squared is negative (which is usually due to
  // numerics).
  CUDA_CALLABLE
  double signed_abs() const
  {
    const double _abs2 = abs2();
    return (_abs2 < 0.0 ? -1.0 : 1.0) * std::sqrt(std::abs(_abs2));
  };
  CUDA_CALLABLE 
  double abs() const { return std::sqrt(abs2()); };
  CUDA_CALLABLE 
  double abs2() const { return (*this) * (*this); };
  CUDA_CALLABLE 
  double p_plus() const { return (*this)[0] + (*this)[3]; }
  CUDA_CALLABLE 
  double p_minus() const { return (*this)[0] - (*this)[3]; }
  CUDA_CALLABLE 
  double p_perp() const { return std::sqrt(p_perp2()); };
  CUDA_CALLABLE 
  double p_perp2() const {
    return (*this)[1] * (*this)[1] + (*this)[2] * (*this)[2];
  }
  CUDA_CALLABLE 
  double m_perp() const { return std::sqrt(m_perp2()); };
  CUDA_CALLABLE 
  double m_perp2() const {
    return p_plus() * p_minus();
  }
  CUDA_CALLABLE
  double rapidity() const {
    return 0.5 * std::log((data[0] + data[3]) / (data[0] - data[3]));
  }
  CUDA_CALLABLE
  Vec4 operator-()
  {
    return Vec4(-(*this)[0], -(*this)[1], -(*this)[2], -(*this)[3]);
  }
  CUDA_CALLABLE 
  Vec4& operator+=(const Vec4& rhs)
  {
    for (int i = 0; i < 4; ++i)
      (*this)[i] += rhs[i];
    return *this;
  }
  CUDA_CALLABLE 
  Vec4& operator-=(const Vec4& rhs)
  {
    for (int i = 0; i < 4; ++i)
      (*this)[i] -= rhs[i];
    return *this;
  }
  CUDA_CALLABLE 
  Vec4& operator*=(double rhs)
  {
    for (int i = 0; i < 4; ++i)
      (*this)[i] *= rhs;
    return *this;
  }
  CUDA_CALLABLE 
  friend Vec4 operator*(Vec4 lhs, double rhs) { return lhs *= rhs; }
  CUDA_CALLABLE 
  friend Vec4 operator*(double lhs, Vec4 rhs) { return rhs *= lhs; }
  CUDA_CALLABLE 
  friend Vec4 operator+(Vec4 lhs, const Vec4& rhs) { return lhs += rhs; }
  CUDA_CALLABLE 
  friend Vec4 operator-(Vec4 lhs, const Vec4& rhs) { return lhs -= rhs; }
  CUDA_CALLABLE 
  friend double operator*(const Vec4& lhs, const Vec4& rhs)
  {
    return lhs[0] * rhs[0] - lhs[1] * rhs[1] - lhs[2] * rhs[2] -
           lhs[3] * rhs[3];
  };

  friend std::ostream& operator<<(std::ostream& o, const Vec4& v)
  {
    return o << "(" << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3]
      << "), m = " << v.signed_abs()
      << ", pT = " << v.p_perp()
      << ", y = " << v.rapidity();
  }

private:
  double data[4];
  
};

#endif
