#include "config.h"
#include "cuda_utilities.h"
#include "histogram.h"
#include "indexing.h"
#include "rambo.h"
#include "rng.h"
#include "vec4.h"
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

int main(int argc, char* argv[])
{
  // Global settings. Note that we do not use parton density functions, such
  // that the longitudinal momentum fractions x_1 and x_2 are simply set to 1.0.
  static const int n_ptcl = 4; // 2 -> (n_ptcl - 2) process
  static const double e_cms = 14000.0; // centre-of-mass energy in GeV
  static const double x1 = 1.0;
  static const double x2 = 1.0;

  static const int batch_size = 1;
  static const int n_events = 1000000;

  // Components of the event generation pipeline:
  // 1. A random number generator `rng`
  // 2. A RAMBO phase-space point generator `rambo`
  // 3. Histograms to bin events `*_histo`
  Rng rng;
  Massless_rambo rambo(e_cms, n_ptcl);
  Histogram y_histo(-5.0, 5.0, 50);
  Histogram pt_histo(0, e_cms / 2.0, 50);

  // Storage for the event generation pipeline:
  // 1. Random numbers `r`
  // 2. Momentum components for all particles `p`
  std::vector<double> r(4 * (n_ptcl - 2));
  std::vector<double> p(n_ptcl * 4);
  std::vector<double> weights(1);

  // This object facilitates indexing momentum components within the common data
  // storage.
  Momentum_indexer<std::vector<double>> mom {p, n_ptcl, batch_size};

  // Start pipeline.
  for (int b = 0; b < n_events; b++) {

    // Generate all random numbers needed by RAMBO.
    rng.fill(r);

    // Generate RAMBO momenta.
    rambo.fill_momenta(p, r, x1, x2);

    // Generate RAMBO phase-space weights.
    rambo.fill_weights(weights, x1, x2);

    // Fill histograms.
    for (int i = 2; i < n_ptcl; i++) {
      Vec4 vec4{mom(i, 0), mom(i, 1), mom(i, 2), mom(i, 3)};
      y_histo.fill(vec4.rapidity(), weights[0]);
      pt_histo.fill(vec4.p_perp(), weights[0]);
    }
  }

  // Normalise histograms.
  y_histo /= n_events;
  pt_histo /= n_events;

  // Output y histo.
  std::ofstream y_file("y.csv");
  y_file << y_histo << '\n';
  y_file.close();

  // Output pT histo.
  std::ofstream pt_file("pt.csv");
  pt_file << pt_histo << '\n';
  pt_file.close();

  return EXIT_SUCCESS;
}
