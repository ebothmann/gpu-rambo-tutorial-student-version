#include "config.h"
#include "cuda_utilities.h"
#include <cstddef>

// Get the momentum component [0, 4) of a particle [0, n_ptcl) for a given event
// [0, batch_size). Modify the implementation to change the access pattern for
// the momentum storage.
template <typename T> class Momentum_indexer {
public:
  CUDA_CALLABLE
	  Momentum_indexer(T &_data, int _n_ptcl, size_t _n_events)
	  : data{_data}, n_ptcl{_n_ptcl}, n_events{_n_events}, event{0} {}
  CUDA_CALLABLE
  double operator()(int ptcl, int component) const {
    return data[event * n_ptcl * 4 + ptcl * 4 + component];
  }
  CUDA_CALLABLE
  double &operator()(int ptcl, int component) {
    return data[event * n_ptcl * 4 + ptcl * 4 + component];
  }
  CUDA_CALLABLE
  void set_event(int _event) { event = _event; }

private:
  T& data;
  int n_ptcl;
  size_t n_events;
  size_t event;
};
